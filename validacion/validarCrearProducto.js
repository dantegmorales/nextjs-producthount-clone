export default function validarCuenta(valores) {
  let errores = {};

  // Validar el nombre del usuario
  if (!valores.nombre) {
    errores.nombre = "El nombre es obligatorio.";
  }
  // Validar el empresa
  if (!valores.empresa) {
    errores.empresa = "Empresa es obligatorio.";
  }
  // Validar url
  if (!valores.url) {
    errores.url = "la URL del producto es obligatoria";
  } else if (!/^(ftp|http|https):\/\/[^ "]+$/.test(valores.url)) {
    errores.url = "URL no válida";
  }
  // Validar el descripcion
  if (!valores.descripcion) {
    errores.descripcion = "Agrega una descripción";
  }

  return errores;
}
