import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
import {
  createUserWithEmailAndPassword,
  getAuth,
  updateProfile,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";

import firebaseConfig from "./config";

class Firebase {
  constructor() {
    const app = initializeApp(firebaseConfig);
    this.auth = getAuth();
    this.db = getFirestore();
    this.storage = getStorage();
  }
  // Registra un usuario
  async registrar(nombre, email, password) {
    const nuevoUsuario = await createUserWithEmailAndPassword(
      this.auth,
      email,
      password
    );

    const usr = await updateProfile(nuevoUsuario.user, {
      displayName: nombre,
    });
    return usr;
  }
  // Inicia sesión del usuario
  async login(email, password) {
    return signInWithEmailAndPassword(this.auth, email, password);
  }

  // Cierra la sesion del usuario
  async cerrarSession() {
    await signOut(this.auth);
  }
}

const firebase = new Firebase();
export default firebase;
