import React, { useState } from "react";
import { css } from "@emotion/react";
import Router from "next/router";

import Layout from "../components/layout/Layout";
import {
  Campo,
  Formulario,
  InputSubmit,
  Error,
} from "../components/ui/Formulario";

import firebase from "../firebase";

// validaciones
import useValidacion from "../hooks/useValidacion";
import validarCuenta from "../validacion/validarCuenta";

const STATE_INICIAL = {
  nombre: "",
  email: "",
  password: "",
};
export default function CrearCuenta() {
  const [error, setError] = useState(false);

  const crearCuenta = async () => {
    try {
      await firebase.registrar(nombre, email, password);
      Router.push("/");
    } catch (error) {
      console.error("error:", error.message);
      setError(error.message);
    }
  };

  const { valores, errores, handleSubmit, handleChange, handleBlur } =
    useValidacion(STATE_INICIAL, validarCuenta, crearCuenta);

  const { nombre, email, password } = valores;
  return (
    <div>
      <Layout>
        <>
          <h1
            css={css`
              text-align: center;
              margin-top: 5rem;
            `}
          >
            Crear Cuenta
          </h1>
          <Formulario onSubmit={handleSubmit} noValidate>
            <Campo>
              <label htmlFor="nombre">Nombre</label>
              <input
                value={nombre}
                onBlur={handleBlur}
                onChange={handleChange}
                type="text"
                id="nombre"
                placeholder="Escribe tu nombre"
                name="nombre"
              />
            </Campo>
            {errores.nombre && <Error>{errores.nombre}</Error>}
            <Campo>
              <label htmlFor="email">Email</label>
              <input
                name="email"
                value={email}
                onBlur={handleBlur}
                onChange={handleChange}
                type="email"
                id="email"
                placeholder="Escribe tu email"
              />
            </Campo>
            {errores.email && <Error>{errores.email}</Error>}
            <Campo>
              <label htmlFor="password">Password</label>
              <input
                name="password"
                value={password}
                onBlur={handleBlur}
                onChange={handleChange}
                type="password"
                id="password"
                placeholder="Escribe tu password"
              />
            </Campo>
            {errores.password && <Error>{errores.password}</Error>}
            {error && <Error>{error}</Error>}
            <InputSubmit type="submit" value="Crear Cuenta" />
          </Formulario>
        </>
      </Layout>
    </div>
  );
}
