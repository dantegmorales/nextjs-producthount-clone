import React, { useState } from "react";
import { css } from "@emotion/react";
import Router from "next/router";

import Layout from "../components/layout/Layout";
import {
  Campo,
  Formulario,
  InputSubmit,
  Error,
} from "../components/ui/Formulario";

import firebase from "../firebase";

// validaciones
import useValidacion from "../hooks/useValidacion";
import validarIniciarSesion from "../validacion/validarIniciarSesion";

const STATE_INICIAL = {
  email: "",
  password: "",
};

export default function Home() {
  const [error, setError] = useState(false);

  const iniciarSesion = async () => {
    try {
      const usuario = await firebase.login( email, password);
      Router.push("/");
    } catch (error) {
      console.error("error:", error.message);
      setError(error.message);
    }
  };

  const { valores, errores, handleSubmit, handleChange, handleBlur } =
    useValidacion(STATE_INICIAL, validarIniciarSesion, iniciarSesion);

  const { email, password } = valores;
  return (
    <div>
      <Layout>
        <>
          <h1
            css={css`
              text-align: center;
              margin-top: 5rem;
            `}
          >
            Iniciar Sesión
          </h1>
          <Formulario onSubmit={handleSubmit} noValidate>
            {errores.nombre && <Error>{errores.nombre}</Error>}
            <Campo>
              <label htmlFor="email">Email</label>
              <input
                name="email"
                value={email}
                onBlur={handleBlur}
                onChange={handleChange}
                type="email"
                id="email"
                placeholder="Escribe tu email"
              />
            </Campo>
            {errores.email && <Error>{errores.email}</Error>}
            <Campo>
              <label htmlFor="password">Password</label>
              <input
                name="password"
                value={password}
                onBlur={handleBlur}
                onChange={handleChange}
                type="password"
                id="password"
                placeholder="Escribe tu password"
              />
            </Campo>
            {errores.password && <Error>{errores.password}</Error>}
            {error && <Error>{error}</Error>}
            <InputSubmit type="submit" value="Iniciar Sesión" />
          </Formulario>
        </>
      </Layout>
    </div>
  );
}
