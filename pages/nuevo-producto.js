import React, { useState, useContext } from "react";
import { css } from "@emotion/react";
import Router, { useRouter } from "next/router";
import { addDoc, collection } from "firebase/firestore";
import { ref } from "firebase/storage";

import Layout from "../components/layout/Layout";
import {
  Campo,
  Formulario,
  InputSubmit,
  Error,
} from "../components/ui/Formulario";

import { FirebaseContext } from "../firebase";

// validaciones
import useValidacion from "../hooks/useValidacion";
import validarCrearProducto from "../validacion/validarCrearProducto";

const STATE_INICIAL = {
  nombre: "",
  empresa: "",
  imagen: "",
  url: "",
  descripcion: "",
};
export default function NuevoProducto() {
  // Context con las operaciones crud de firebase
  const { usuario, firebase } = useContext(FirebaseContext);

  // react-journal

  // States
  const [error, setError] = useState(false);

  const [urlImage, setUrlImage] = useState("");

  const onChange = async (e) => {
    let formData = new FormData();
    const file = e.target.files[0];
    formData.append("file", file);
    formData.append("upload_preset", "react-journal");
    const resp = await fetch(
      "https://api.cloudinary.com/v1_1/dante-g-morales/upload",
      {
        method: "POST",
        body: formData,
      }
    );
    if (resp.ok) {
      const cloudResponse = await resp.json();
      setUrlImage(cloudResponse.secure_url);
    }
  };

  // hook de router para redireccionar
  const router = useRouter();

  const CrearProducto = async () => {
    // si el usario no esta autenticado llevar a login
    if (!usuario) {
      return router.push("/login");
    }
    // crear el objeto de nuevo producto
    const producto = {
      nombre,
      empresa,
      url,
      urlImage,
      descripcion,
      votos: 0,
      comentarios: [],
      creado: Date.now(),
    };
    // insertar en la BD
    try {
      const docRef = await addDoc(
        collection(firebase.db, "productos"),
        producto
      );
      return router.push("/");
    } catch (e) {
      console.error("Error adding document: ", e);
    }
  };

  const { valores, errores, handleSubmit, handleChange, handleBlur } =
    useValidacion(STATE_INICIAL, validarCrearProducto, CrearProducto);

  const { nombre, empresa, imagen, url, descripcion } = valores;
  return (
    <div>
      <Layout>
        <>
          <h1
            css={css`
              text-align: center;
              margin-top: 5rem;
            `}
          >
            Nuevo Producto
          </h1>
          <Formulario onSubmit={handleSubmit} noValidate>
            <fieldset>
              <legend>Información General</legend>
              <Campo>
                <label htmlFor="nombre">Nombre</label>
                <input
                  value={nombre}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  id="nombre"
                  placeholder="Escribe tu nombre"
                  name="nombre"
                />
              </Campo>
              {errores.nombre && <Error>{errores.nombre}</Error>}
              <Campo>
                <label htmlFor="empresa">Empresa</label>
                <input
                  value={empresa}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  id="empresa"
                  placeholder="Nombre de la empresa o compañia"
                  name="empresa"
                />
              </Campo>
              {errores.empresa && <Error>{errores.empresa}</Error>}
              <input
                accept="image/*"
                onChange={onChange}
                type="file"
                id="image"
                name="image"
              />
              {errores.imagen && <Error>{errores.imagen}</Error>}
              <Campo>
                <label htmlFor="url">URL</label>
                <input
                  value={url}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  placeholder="URL de tu producto"
                  type="url"
                  id="url"
                  name="url"
                />
              </Campo>
              {errores.url && <Error>{errores.url}</Error>}
            </fieldset>
            <fieldset>
              <legend>Sobre tu producto</legend>
              <Campo>
                <label htmlFor="descripcion">Descripción</label>
                <textarea
                  value={descripcion}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  id="descripcion"
                  name="descripcion"
                />
              </Campo>
              {errores.descripcion && <Error>{errores.descripcion}</Error>}
            </fieldset>
            {error && <Error>{error}</Error>}
            <InputSubmit type="submit" value="Crear Producto" />
          </Formulario>
        </>
      </Layout>
    </div>
  );
}
